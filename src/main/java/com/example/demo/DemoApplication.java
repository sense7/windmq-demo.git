package com.example.demo;

import com.stanwind.wmqtt.WMHolder;
import com.stanwind.wmqtt.anno.EnableWindMQ;
import com.stanwind.wmqtt.auth.domain.MQTTClientConnData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Collections;

/**
 * DemoApplication
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2021-02-03 3:21 PM
 **/
@EnableWindMQ
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class DemoApplication {

    private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext run = SpringApplication.run(DemoApplication.class, args);
        log.info("<<<<<<DemoApplication is running......>>>>>>");
        WMHolder bean = run.getBean(WMHolder.class);
        MQTTClientConnData clientConnData = bean.getClientConnData(Collections.singletonList("IOT_CLIENT/cmd/#"), null);
        System.out.println(clientConnData);
    }
}
