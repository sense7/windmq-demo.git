package com.example.demo.domain;

import java.io.Serializable;

/**
 * Connected
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2020-11-12 11:29
 **/
public class Connected implements Serializable {

    private String username;
    private Long ts;
    private Integer sockport;
    private Integer proto_ver;
    private String proto_name;
    private Integer keepalive;
    private String ipaddress;
    private Long expiry_interval;
    private Long connected_at;
    private Integer connack;
    private String clientid;
    private Boolean clean_start;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public Integer getSockport() {
        return sockport;
    }

    public void setSockport(Integer sockport) {
        this.sockport = sockport;
    }

    public Integer getProto_ver() {
        return proto_ver;
    }

    public void setProto_ver(Integer proto_ver) {
        this.proto_ver = proto_ver;
    }

    public String getProto_name() {
        return proto_name;
    }

    public void setProto_name(String proto_name) {
        this.proto_name = proto_name;
    }

    public Integer getKeepalive() {
        return keepalive;
    }

    public void setKeepalive(Integer keepalive) {
        this.keepalive = keepalive;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    public Long getExpiry_interval() {
        return expiry_interval;
    }

    public void setExpiry_interval(Long expiry_interval) {
        this.expiry_interval = expiry_interval;
    }

    public Long getConnected_at() {
        return connected_at;
    }

    public void setConnected_at(Long connected_at) {
        this.connected_at = connected_at;
    }

    public Integer getConnack() {
        return connack;
    }

    public void setConnack(Integer connack) {
        this.connack = connack;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public Boolean getClean_start() {
        return clean_start;
    }

    public void setClean_start(Boolean clean_start) {
        this.clean_start = clean_start;
    }
}

