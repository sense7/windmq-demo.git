package com.example.demo.domain;

import java.io.Serializable;

/**
 * {
 *   "username": "demo",
 *   "ts": 1605063068400,
 *   "reason": "normal",
 *   "disconnected_at": 1605063068400,
 *   "clientid": "d12f47491f504663b69aee08c472e6a1"
 * }
 */
public class Disconnected implements Serializable {
    private String username;
    private Long ts;
    private String reason;
    private String clientid;
    private Long disconnected_at;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public Long getDisconnected_at() {
        return disconnected_at;
    }

    public void setDisconnected_at(Long disconnected_at) {
        this.disconnected_at = disconnected_at;
    }
}
