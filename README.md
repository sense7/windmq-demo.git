# windmq - MQTT快速开发脚手架 使用DEMO
### 前言
快速开发处理MQTT topic，一个方法注解就搞定

#### 工程源码
https://gitee.com/sense7/windmq.git

#### 依赖示例
```xml
    <!-- windmq dependency -->
    <dependency>
      <groupId>com.stanwind</groupId>
      <artifactId>spring-boot-windmq</artifactId>
      <version>1.0.0-RELEASE</version>
    </dependency>

    <!-- MQTT -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-integration</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.integration</groupId>
      <artifactId>spring-integration-mqtt</artifactId>
      <exclusions>
        <exclusion>
          <artifactId>org.eclipse.paho.client.mqttv3</artifactId>
          <groupId>org.eclipse.paho</groupId>
        </exclusion>
      </exclusions>
    </dependency>
    <!-- 1.2.0 版本有bug -->
    <dependency>
      <groupId>org.eclipse.paho</groupId>
      <artifactId>org.eclipse.paho.client.mqttv3</artifactId>
      <version>1.2.1</version>
    </dependency>
  </dependencies>
```
